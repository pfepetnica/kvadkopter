class Actuation
{
  float V = 0;
}

class Measurement
{
  float theta = 0;
}

class MojaSimulacija extends Simulation
{
  float V=0;   // napon
     
  float dt=0;   //dt
  
  float l=1;   // duzina stapa
  float m=1;   // masa stapa
  
  float k=0.1;   // koeficijent proporcije napona i sile
  
  float Inercija=(1.0/12)*(m*(l*l));   //Momemenet inercije
  
  float vreme=0;   //vreme u sistemu
  
  float start=5;   //vreme kada pocinje da radi
  
  float ddtheta = 0;   // ''ugao
  float dtheta = -0.3;   // 'ugao
  float theta = 0.63598775;   // ugao
  
  float x1=250; 
  float y1=180; 
  float x2=390; 
  float y2=180;  //koordinate krajeva 
  
  float ref=0;
  float ulaz=0;
  float pulaz=0;
  float feedback=0;
  
  float G=0.3; //gain   
 
  MojaSimulacija ()
  {
    SetUpdateMul(10);
    
    SetControlMul(5);
    
    dt = GetUpdatePeriod();
  }
  
  void Update(Actuation input)
  {
    vreme=vreme+dt;
    
    ddtheta =  (k*l*V) / Inercija;
    dtheta += ddtheta * dt;
    theta  += dtheta  * dt;
    
    x1=320+(390-320)*cos(PI+theta);
    y1=180+(390-320)*sin(PI+theta);
    x2=320+(390-320)*cos(theta);
    y2=180+(390-320)*sin(theta);
    
    if(y1>230){theta-=dtheta*dt; dtheta=0;}
    if(y2>230){theta-=dtheta*dt; dtheta=0;}
    
    feedback=theta;
    
  }

  Measurement Measure()
  {
    Measurement m = new Measurement();
    m.theta = theta;
    return m;
  }

  Actuation Control(Measurement m)
  {
   Actuation o = new Actuation();
   
   ulaz=(ref-feedback)*G;
   V=(ulaz*1-(pulaz-ulaz)/(dt/10));
   pulaz=ulaz;


   return o;
  }

  void Visualize()
  {
    background(200);
    stroke(255, 0, 0);

    rect(1,230,640,360);
    line(x1,y1,x2,y2);
  }
}

void InitSimulation()
{  
  surface.setSize(640, 360);
  stroke(255);
  sim = new MojaSimulacija();
}